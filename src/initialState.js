const initialState = {
    todos: [

      { text: 'Homework 1', // index 0
      completed: true
      },

      { text: 'Homework2', //index 1
      completed: false
      },
    
      { text: 'Homework3', //index 2
      completed: false
      },

      { text: 'Homework4',
      completed: false
      },

      {text: 'Homework5',
      completed: false
      },
      
    ]
  };

export default initialState;