import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';
import initialState from '../src/initialState';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';

export default function configureStore() {
  const middleware = [
    thunk,
  ]
  return createStore(
    rootReducer,
    initialState,
    composeWithDevTools(
      applyMiddleware(...middleware),
    )
  );
}
