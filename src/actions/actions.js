export const updateTodosAction = (todos) => (dispatch) => {
  dispatch({
    type: 'UPDATE_TODOS',
    payload: { todos },
  })
};


