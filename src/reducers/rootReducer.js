import { combineReducers } from 'redux';
import todoReducer from './todoReducer'; 
import addItemReducer from './addItemReducer';

export default combineReducers ({
  todos: todoReducer,
  item: addItemReducer
});
