import React from 'react';
import PropTypes from 'prop-types';
import classNames from './Start.module.scss';


class Header extends React.PureComponent {
    render() {
      return (
        <div>
        <header className={classNames.menuHeader}>
        <div className={classNames.hamburger}></div>
        <div className={classNames.hamburger}></div>
        <div className={classNames.hamburger}></div>
    </header>

    <h1 className={classNames.mainPageName}>D A I L I S T</h1>
    <sub-header><h2 className={classNames.subHeaders}> UPCOMING</h2></sub-header>
    </div>
      );
    } 
  }
export default Header;

