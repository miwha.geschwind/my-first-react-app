import React from 'react';
import PropTypes from 'prop-types';
import classNames from './Start.module.scss';

class TodoItem extends React.PureComponent {
  static propTypes = {
    todo: PropTypes.shape({
      text: PropTypes.string,
      completed: PropTypes.bool,
    }),
    index: PropTypes.number,
    onClick: PropTypes.func,
    updatedIndex: PropTypes.number,
  }
  render() {
    const {
      index,
      todo,
      onClick,
      updatedIndex
    } = this.props;
    console.log(todo)
    return (
      <button 
        onClick={() => onClick(index)}
        key={index}
        className={(todo.completed) ? classNames.buttonCompleted : classNames.upcomingTask }
        >
        {`${updatedIndex}. ${todo.text}`}
      </button>
    );
  }
  
};

export default TodoItem;