import React from 'react';
import classNames from './Start.module.scss';
import { styles } from 'ansi-colors';
import { arrayExpression } from '@babel/types';
import TodoItem from './TodoItem';
import Header from './Header';
import AddTodoButton from './AddTodoButton';
import { connect } from 'react-redux';
import { updateTodosAction } from '../../../actions/actions';


// class componentt
class Start extends React.PureComponent {

  handleButtonClick = (e) => {
    const result = prompt('What is the task you would like to add?', e.target); // i knew that i wanted the target
    console.log(result);
    this.setState({todos: [...this.props.todos, {
      text: result,
      completed: false,
    }]}, () => {
      console.log(this.state)
    })

  }


  handleTodoClick = (i) => {
    const { todos } = this.props;
    const newArray = [...todos];
    // newArray.splice(index,1)
    if(newArray[i].completed === true) {
      newArray[i].completed = false;
    } else {
      newArray[i].completed = true;
    }

    this.props.updateTodosAction(newArray)
  }

  showCompletedTodos= () => {
    const { todos } = this.props; //destructuring
    const array =[]
    var j = 0;
    for(let index=0; index<todos.length; index++){

      if (todos[index].completed === true){ 
      j++;
      array.push(
      <TodoItem
          key={`todoItem`+index}
          todo={todos[index]}
          index={index}
          onClick={this.handleTodoClick}
          updatedIndex={j}
        />)
      }
    }
      return array;
    }

  showUncompletedTodos = () => {
    // const todos = this.state.todos;
    const { todos } = this.props;
    const array =[]
    var j = 0;
    for(let index=0; index<todos.length; index++){
      if (todos[index].completed !=true){ 
      j++;
      array.push(
        <TodoItem
          key={`todoItem`+index}
          todo={todos[index]}
          index={index}
          onClick={this.handleTodoClick}
          updatedIndex={j}
        />
        )
      }
    }
      return array;
    }



  render() {

  console.log(this.props);
    return (
      <div className={classNames.root}>
        <Header/>
        <div className={classNames.upcomingTaskDiv}>
          {this.showUncompletedTodos()}
        </div>
        <br></br>

        <h2 className={classNames.subHeaders}> FINISHED</h2>
  
        <div className={classNames.buttonCompletedDiv}>
          {this.showCompletedTodos()}
        </div>
        <br></br>
        <br></br>
        <AddTodoButton 
        onClick = {this.handleButtonClick}/>
       </div>

    );
  }
};


const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch => ({
    updateTodosAction: (todos) => dispatch(updateTodosAction(todos)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Start);
