import React from 'react';
import PropTypes from 'prop-types';
import classNames from './Start.module.scss';
import { connect } from 'react-redux';
import { addTodoItemAction} from '../../../actions/addItemAction';
import { styles } from '../../../styles/global.scss'

class AddTodoButton extends React.PureComponent {
  static propTypes = {
    onClick: PropTypes.func
  }
  
  render() {
    const { 
      onClick
    } = this.props;
    
    return (
      <button 
      onClick={(e) => this.props.onClick(e)}
      className={classNames.addNewTodo}>+</button> 
    );
  } 
}

const mapStateToProps = state => ({
  item: state.item,
});

const mapDispatchToProps = dispatch => ({
  addTodoItemAction: (item) => dispatch(addTodoItemAction(item)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTodoButton);


