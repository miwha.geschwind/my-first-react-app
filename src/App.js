import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom"; // https://reacttraining.com/react-router/web/example/basic - read up on react-router
import Start from '../src/components/Pages/Start';



// functional components

class App extends Component {
  render() {
    return (
      <div className="App">
      <Router>
        <Route exact path="/" component={Start} />
      </Router>
      </div>
    );
  }
}

export default App;